import { AngularFireAuth } from 'angularfire2/auth';
import { Message } from './../../model/message.interface';
import { MESSAGE_LIST } from './../../mockup/message.mockup';
import { Profile } from './../../model/profile.interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs';

/**
 * Generated class for the MessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {

  peerProf: Profile;
  msg: string;
  msgList: Message[] = MESSAGE_LIST;
  myid: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private afAuth: AngularFireAuth) {
    this.peerProf = navParams.get('peerProf');
    this.myid = this.afAuth.auth.currentUser.uid;
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagePage');
  }

  send() {

  }
}
